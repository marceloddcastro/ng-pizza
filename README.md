Usei o angular-cli para criar o projeto, criei os seguintes arquivos em app:
      "app.service.ts", onde centralizei o acesso ao webservice.
      "order.model.ts", este representa um novo pedido.
      "app.routes", este é onde eu defini as rotas da aplicação,
      são apenas duas, uma para listar e outra para criar uma nova ordem.

O resto do projeto não tem nada demais, tem um componente para criar uma
nova ordem e um para listar, apenas isso.


Eu fiz um build deste projeto "ng build --prod", e colei
dentro do projeto gerado com express-generator, ai tem uma rota que 
serve este projeto do Angular.

Acredito que vc vai precisar mandar este fonte para seu professor também.

