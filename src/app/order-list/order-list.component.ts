import {Component, OnInit} from '@angular/core';
import {AppService} from 'app/app.service';
import {Order} from "../order.model";
@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  orders: Order[] = [];

  address: string;
  phone: string;

  constructor(private appService: AppService) {
  }

  ngOnInit() {
    this.appService.listOrders().subscribe(orders => this.orders = orders);
  }

  search() {
    if (this.address || this.phone)
      this.appService.listOrder(this.address || '', this.phone || '').subscribe(orders => this.orders = orders);
    else
      this.ngOnInit();
  }

}
