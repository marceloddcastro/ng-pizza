export class Order {
  public client: Client;
  public pizzas: Pizza[];
  public tax: number;
  public total: number;

  constructor() {
    this.client = new Client();
    this.pizzas = [];
    this.tax = 0;
    this.total = 0;
  }
}

export class Client {
  public name: string;
  public email: string;
  public address: string;
  public telephone: string;

  constructor() {
  }
}

export class Pizza {
  public description: string;
  public price: string;
  public qty: number;
  public subTotal: number;
  public toppings: Topping[];
  public total: number;

  constructor() {
    this.toppings = [];
    this.subTotal = 0;
    this.total = 0;
  }
}

export class Topping {
  public description: string;
  public price: number;

  constructor() {
  }
}
