import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {environment} from '../environments/environment';
import {Observable, Subject} from "rxjs";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
@Injectable()
export class AppService {
  apiBaseUrl: string = environment.apiUrl;

  constructor(private http: Http) {
  }

  listPizzas(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}pizzas`)
      .map((response: Response) => response.json());
  }

  listToppings(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}toppings`)
      .map((response: Response) => response.json());
  }

  listOrders(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}orders`)
      .map((response: Response) => response.json());
  }

  listOrder(address: string, phone: string): Observable<any> {
    return this.http.post(`${this.apiBaseUrl}searchOrder`, {address: address, phone: phone})
      .map((response: Response) => response.json());
  }

  createOrder(order: any): Observable<any> {
    return this.http.post(`${this.apiBaseUrl}order`, order)
      .map((response: Response) => response.json());
  }
}
