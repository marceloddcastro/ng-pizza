import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import {AppService} from './app.service';
import {AppRoutes} from './app.routes';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderComponent } from './order/order.component';
@NgModule({
  declarations: [
    AppComponent,
    OrderListComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutes,
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
