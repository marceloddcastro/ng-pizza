import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { OrderComponent } from "app/order/order.component";
import { OrderListComponent } from "app/order-list/order-list.component";

export const routes: Routes = [
    { path: '', component: OrderListComponent },
    { path: 'order', component: OrderComponent }
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
