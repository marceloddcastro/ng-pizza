import {Component, OnInit} from '@angular/core';
import {AppService} from "app/app.service";
import {Order} from "app/order.model";
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  order: Order = new Order();
  pizzas = [];

  toppings = [];
  pizza: any;
  size: any;
  qty: number = 1;

  constructor(private appService: AppService, private router: Router) {
  }

  ngOnInit() {
    this.appService.listPizzas()
      .subscribe(pizzas => this.pizzas = pizzas);

    this.appService.listToppings()
      .subscribe(toppings => this.toppings = toppings);
  }

  subTotal(): number {
    if (!this.pizza || !this.size || !this.qty) return 0;

    let total = this.size.price * this.qty;
    for (let topping of this.toppings) {
      if (topping.selected) total += topping.price;
    }
    return total;
  }

  isValidTouched(control: FormControl): boolean {
    return (!control.valid && control.touched);
  };

  applyCssErro(control: FormControl) {
    return {
      'has-danger': this.isValidTouched(control),
      'form-control-danger': this.isValidTouched(control)
    };
  }

  send() {
    this.order.pizzas.push({
      description: this.pizza.description,
      price: this.size.price,
      qty: this.qty,
      subTotal: this.size.price * this.qty,
      toppings: this.toppings.filter(val => val.selected),
      total: this.subTotal()
    });

    this.appService.createOrder(this.order)
      .subscribe(result => {
        window.alert(result);
        this.router.navigate(['/']);
      }, err => window.alert(err));
  }

}
