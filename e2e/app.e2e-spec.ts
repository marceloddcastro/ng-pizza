import { NgPizzaPage } from './app.po';

describe('ng-pizza App', () => {
  let page: NgPizzaPage;

  beforeEach(() => {
    page = new NgPizzaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
